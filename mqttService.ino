#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Update these with values suitable for your network.
byte mac[] = {  0xDE, 0xED, 0xBA, 0xFE, 0xFE, 0xED };
IPAddress ip(10, 0, 0, 190);
IPAddress server(10, 0, 0, 109);

const char* ssid = "";
const char* password = "";

String incommingTopic = "";

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String value = "";
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    value = value + (char)payload[i];
  }
  incommingTopic = String(topic);
  Serial.println("");
  publishState(value);

  if (incommingTopic.indexOf("dim") >= 0) {
    changeDim(value);
  }else if (incommingTopic.indexOf("type") >= 0) {
    changeType(value);
  }else if (incommingTopic.indexOf("speed") >= 0) {
    changeSpeed(value);
  }  
}

WiFiClient espClient;
PubSubClient client(server, 1883, callback, espClient);

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("MoodLamp", "","")) {
      Serial.println("connected");
      delay(5000);
      subscribeTopics();
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void initMqttService() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void mqttListener() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}

void publishState(String payload) {
  String publicTopic = incommingTopic;
  publicTopic.replace("/set", "");
  String publicPayload = "OFF";
  if (payload == "ON") {
    publicPayload = "ON";
  }
  client.publish(string2char(publicTopic), string2char(publicPayload));
}

void subscribeTopics() {
  Serial.println("Subscribing to: " + dimTopic);
  client.subscribe(string2char(dimTopic));

  Serial.println("Subscribing to: " + typeTopic);
  client.subscribe(string2char(typeTopic));

  Serial.println("Subscribing to: " + speedTopic);
  client.subscribe(string2char(speedTopic));
}

void publishMessage(char* topic, char* payload) {
  mqttListener();
  Serial.print("Pusblish [");
  Serial.print(topic);
  Serial.print("] ");
  Serial.print(payload);
  Serial.println("");
  client.publish(topic, payload);
}

char* string2char(String command) {
  if (command.length() != 0) {
    char *p = const_cast<char*>(command.c_str());
    return p;
  }
}
