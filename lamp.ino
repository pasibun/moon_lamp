#include "FastLED.h"
#include "lampApplication.h"
#include <pixeltypes.h>

// Number of RGB LEDs in the strand
#define NUM_LEDS 89

// Define the array of leds
CRGBArray<NUM_LEDS> leds;
// Arduino pin used for Data
#define PIN 6
#define POT 0
#define BTN 16

#define MAX_BRIGHTNESS  220      // Thats full on, watch the power!
#define MIN_BRIGHTNESS  5       // set to a minimum of 25%
#define MAX_SPEED       2500
#define MIN_SPEED       10

int potValue = 0;
int buttonState = 0;
int speedValue = 20;
int currentOptie = 0;
int currentBrightness = 0;
boolean firstTime = true;

void setup() {
  Serial.begin(115200);
  pinMode(BTN, INPUT);
  Serial.println("Starting application..");
  initMqttService();
  FastLED.addLeds<NEOPIXEL, PIN>(leds, NUM_LEDS);
  FastLED.setBrightness(MIN_BRIGHTNESS);
  FastLED.show();
}


void loop() {
  mqttListener();
  checkBtnState();
  checkPotValue();

  if (firstTime) {
    oneAtATime(random8());
    firstTime = false;
  }

  switch (currentOptie) {
    case 1: oneAtATime(random8());
      break;
    case 2: growingBar(random8());
      break;
    case 3: rainbow_wave(10, 10); //rainbowCycle();
      break;
    case 4: fire(55, 120, 15);
      break;
    case 5: meteorRain(0xff, 0xff, 0xff, 10, 64, true, 30);
      break;
    case 6: RunningLights(0xff, 0xff, 0x00, 50);
      break;
    default: FastLED.clear();
      showStrip();
      break;
    case 99: fadeInOut(random8());
      break;
  }
  showStrip();
  delay(5);
}

void checkBtnState() {
  buttonState = digitalRead(BTN);
  if (buttonState == HIGH) {
    if (currentOptie > -1 and currentOptie < 7) {
      currentOptie++;
    } else {
      currentOptie = 0;
    }
    Serial.print("Changing current option to: ");
    Serial.println(currentOptie);
  }
}

void checkPotValue() {
  int difference = map(analogRead(POT), 0, 1023, 0, 255) - currentBrightness;
  //Serial.println(difference);
  if (difference > 30 or difference < -30) {
    currentBrightness = map(analogRead(POT), 0, 1023, 0, 255);
    FastLED.setBrightness(constrain(currentBrightness, MIN_BRIGHTNESS, MAX_BRIGHTNESS));
  }
}

void changeType(String payload) {
  currentOptie = payload.toInt();
}

//todo miss met de knop switchen tussen dim/snelheid
void changeDim(String payload) {
  int brightness = payload.toInt();
  if (brightness < 256 and brightness > 0) {
    FastLED.setBrightness(brightness);
    showStrip();
  }
}

void changeSpeed(String payload) {
  speedValue = constrain(payload.toInt(), MIN_SPEED, MAX_SPEED);
}
//Optie 1
void oneAtATime(uint8_t color) {
  for (int i = 0 ; i < NUM_LEDS; i++ ) {
    leds[i] = CHSV(color, 255, 255);
    showStrip();
    leds[i] = CRGB::Black;
    delay(speedValue);
  }
}
// Optie 2
void growingBar(uint8_t color) {
  for (int i = 0 ; i < NUM_LEDS; i++ ) {
    leds[i] = CHSV(color, 255, 255);
    showStrip();
    delay(speedValue);
  }
  for (int i = NUM_LEDS - 1 ; i >= 0; i-- ) {
    leds[i] = CRGB::Black;
    FastSPI_LED.show();
    delay(1);
  }
}

//weg?
void fadeInOut(uint8_t color) {
  for (int k = 0; k < 255; k++) {
    for (int i = 0; i < NUM_LEDS; i++ ) {
      leds[i] = CHSV(color, 255, k);
    }
    FastLED.show();
    delay(3);
  }
  for (int k = 255; k >= 0; k--) {
    for (int i = 0; i < NUM_LEDS; i++ ) {
      leds[i] = CHSV(color, 255, k);
    }
    showStrip();
    delay(3);
  }
}

//Optie 3
void rainbow_wave(uint8_t thisSpeed, uint8_t deltaHue) {
  uint8_t thisHue = beatsin8(thisSpeed, 0, 255);              // A simple rainbow wave.
  //uint8_t thisHue = beat8(thisSpeed, 255);                    // A simple rainbow march.

  fill_rainbow(leds, NUM_LEDS, thisHue, deltaHue);
  showStrip();
}



void rainbowCycle() {
  byte *c;
  uint16_t i, j;

  for (j = 0; j < 256 * 5; j++) { // 5 cycles of all colors on wheel
    for (i = 0; i < NUM_LEDS; i++) {
      c = Wheel(((i * 256 / NUM_LEDS) + j) & 255);
      setPixel(i, *c, *(c + 1), *(c + 2));
    }
    showStrip();
    delay(speedValue);
  }
}

byte * Wheel(byte WheelPos) {
  static byte c[3];
  if (WheelPos < 85) {
    c[0] = WheelPos * 3;
    c[1] = 255 - WheelPos * 3;
    c[2] = 0;
  } else if (WheelPos < 170) {
    WheelPos -= 85;
    c[0] = 255 - WheelPos * 3;
    c[1] = 0;
    c[2] = WheelPos * 3;
  } else {
    WheelPos -= 170;
    c[0] = 0;
    c[1] = WheelPos * 3;
    c[2] = 255 - WheelPos * 3;
  }
  return c;
}

//Optie 4
void fire(int Cooling, int Sparking, int SpeedDelay) {
  static byte heat[NUM_LEDS];
  int cooldown;

  // Step 1.  Cool down every cell a little
  for ( int i = 0; i < NUM_LEDS; i++) {
    cooldown = random(0, ((Cooling * 10) / NUM_LEDS) + 2);
    if (cooldown > heat[i]) {
      heat[i] = 0;
    } else {
      heat[i] = heat[i] - cooldown;
    }
  }
  // Step 2.  Heat from each cell drifts 'up' and diffuses a little
  for ( int k = NUM_LEDS - 1; k >= 2; k--) {
    heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2]) / 3;
  }

  // Step 3.  Randomly ignite new 'sparks' near the bottom
  if ( random(255) < Sparking ) {
    int y = random(7);
    heat[y] = heat[y] + random(160, 255);
    //heat[y] = random(160,255);
  }

  // Step 4.  Convert heat to LED colors
  for ( int j = 0; j < NUM_LEDS; j++) {
    setPixelHeatColor(j, heat[j] );
  }
  showStrip();
  delay(SpeedDelay);
}

void setPixelHeatColor (int Pixel, byte temperature) {
  // Scale 'heat' down from 0-255 to 0-191
  byte t192 = round((temperature / 255.0) * 191);

  // calculate ramp up from
  byte heatramp = t192 & 0x3F; // 0..63
  heatramp <<= 2; // scale up to 0..252

  // figure out which third of the spectrum we're in:
  if ( t192 > 0x80) {                    // hottest
    setPixel(Pixel, 255, 255, heatramp);
  } else if ( t192 > 0x40 ) {            // middle
    setPixel(Pixel, 255, heatramp, 0);
  } else {                               // coolest
    setPixel(Pixel, heatramp, 0, 0);
  }
}

//Optie 5
void meteorRain(byte red, byte green, byte blue, byte meteorSize, byte meteorTrailDecay, boolean meteorRandomDecay, int SpeedDelay) {
  setAll(0, 0, 0);
  for (int i = 0; i < NUM_LEDS + NUM_LEDS; i++) {
    // fade brightness all LEDs one step
    for (int j = 0; j < NUM_LEDS; j++) {
      if ( (!meteorRandomDecay) || (random(10) > 5) ) {
        fadeToBlack(j, meteorTrailDecay );
      }
    }

    // draw meteor
    for (int j = 0; j < meteorSize; j++) {
      if ( ( i - j < NUM_LEDS) && (i - j >= 0) ) {
        setPixel(i - j, red, green, blue);
      }
    }
    showStrip();
    delay(SpeedDelay);
  }
}

//Optie 6
void RunningLights(byte red, byte green, byte blue, int WaveDelay) {
  int Position = 0;

  for (int j = 0; j < NUM_LEDS * 2; j++)
  {
    Position++; // = 0; //Position + Rate;
    for (int i = 0; i < NUM_LEDS; i++) {
      // sine wave, 3 offset waves make a rainbow!
      //float level = sin(i+Position) * 127 + 128;
      //setPixel(i,level,0,0);
      //float level = sin(i+Position) * 127 + 128;
      setPixel(i, ((sin(i + Position) * 127 + 128) / 255)*red,
               ((sin(i + Position) * 127 + 128) / 255)*green,
               ((sin(i + Position) * 127 + 128) / 255)*blue);
    }

    showStrip();
    delay(WaveDelay);
  }
}

void fadeToBlack(int ledNo, byte fadeValue) {
  leds[ledNo].fadeToBlackBy( fadeValue );
}


void setPixel(int Pixel, byte red, byte green, byte blue) {
  leds[Pixel].r = red;
  leds[Pixel].g = green;
  leds[Pixel].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++ ) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}

void showStrip() {
  FastLED.show();
}
